export default interface IImageItem {
    description: string;
    versions: string[];
}
